#include "Customer.hpp"

Customer::Customer()
{
    custNo = 0;
}

Customer::Customer(const int custNoToSet, const std::string& custNameToSet, const std::string& custEmailToSet)
{
    custNo = custNoToSet;
    custName = custNameToSet;
    custEmail = custEmailToSet;
}

void Customer::readData()
{
    std::cout << "Please input the following customer information:\n"
              << "    Customer number: " << std::flush;
    std::cin.sync();
    std::cin >> custNo;
    std::cout << "    Full name: " << std::flush;
    std::cin.sync();
    getline(std::cin, custName);
    std::cout << "    Email: " << std::flush;
    std::cin.sync();
    std::cin >> custEmail;
}

void Customer::setData(int custNoToSet, const std::string& custNameToSet, const std::string& custEmailToSet)
{
    custNo = custNoToSet;
    custName = custNameToSet;
    custEmail = custEmailToSet;
}

void Customer::printData(std::ostream& stream = std::cout) const
{
    stream << "Customer data:\n"
              << "    - Number: " << custNo << '\n'
              << "    - Full name: " << custName << '\n'
              << "    - Email: " << custEmail << std::endl;
}

int Customer::getCustNo() const
{
    return custNo;
}

const std::string& Customer::getCustName() const
{
    return custName;
}

const std::string& Customer::getCustEmail() const
{
    return custEmail;
}
