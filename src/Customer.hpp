#ifndef ANTIKEIMENOSTRAFES_PROJECT_CUSTOMER_HPP
#define ANTIKEIMENOSTRAFES_PROJECT_CUSTOMER_HPP

class Customer {
private:
    int custNo;
    std::string custName;
    std::string custEmail;

public:
    Customer();
    Customer(int custNoToSet, const std::string& custNameToSet, const std::string& custEmailToSet);

    void readData();
    void setData(int custNoToSet, const std::string& custNameToSet, const std::string& custEmailToSet);
    void printData(std::ostream& stream) const;
    int getCustNo() const;
    const std::string& getCustName() const;
    const std::string& getCustEmail() const;
};

#include "Customer.cpp"

#endif //ANTIKEIMENOSTRAFES_PROJECT_CUSTOMER_HPP
