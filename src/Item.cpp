#include "Item.hpp"

Item::Item()
{
    itemNo = 0;
    itemPrice = 0;
}

Item::Item(int itemNoToSet, const std::string& itemDescToSet, float itemPriceToSet)
{
    itemNo = itemNoToSet;
    itemDesc = itemDescToSet;
    itemPrice = itemPriceToSet;
}

Item::Item(std::ifstream& input)
{
    readData(input);
}

void Item::readData(std::ifstream& input)
{
    input >> itemNo;
    input.get(); // Skipping over the space right after the number that we know always exists, thus potentially skipping trimming the start and saving many moves

    std::getline(input, itemDesc);
    if (itemDesc.empty())
        throw std::runtime_error("Encountered incorrectly formatted line, couldn't find a description");
    // Trimming the end
    while (itemDesc.back()==' ')
        itemDesc.pop_back();
    if (itemDesc.empty())
        throw std::runtime_error("Encountered incorrectly formatted line, couldn't find a description");
    // Extracting the price
    unsigned i = itemDesc.length()-1;
    while (itemDesc[i]!=' ' && i!=0)
        --i;
    if (i==0)
        throw std::runtime_error("Encountered incorrectly formatted line, couldn't find the price");
    itemPrice = std::stof(itemDesc.substr(i+1)); // +1 Because i will stop on the ' ' just before the price

    itemDesc.erase(i); // Deleting the price and the trailing space
    // Trimming the end again
    while (itemDesc.back()==' ')
        itemDesc.pop_back();
    // Trimming the start
    i = 0;
    while (itemDesc[i]==' ')
        ++i;
    itemDesc.erase(0, i);
}

void Item::setData(int itemNoToSet, const std::string& itemDescToSet, float itemPriceToSet)
{
    itemNo = itemNoToSet;
    itemDesc = itemDescToSet;
    itemPrice = itemPriceToSet;
}

void Item::printData(std::ostream& stream = std::cout) const
{
    stream << itemNo << "  " << itemDesc << "    " << itemPrice;
}

int Item::getItemNo() const
{
    return itemNo;
}

const std::string& Item::getItemDesc() const
{
    return itemDesc;
}

float Item::getItemPrice() const
{
    return itemPrice;
}
