#ifndef ANTIKEIMENOSTRAFES_PROJECT_ITEM_HPP
#define ANTIKEIMENOSTRAFES_PROJECT_ITEM_HPP

#include <fstream>

class Item {
private:
    int itemNo;
    std::string itemDesc;
    float itemPrice;
public:
    Item();
    Item(int itemNoToSet, const std::string& itemDescToSet, float itemPriceToSet);
    explicit Item(std::ifstream& input);

    void readData(std::ifstream& input);
    void setData(int itemNoToSet, const std::string& itemDescToSet, float itemPriceToSet);
    void printData(std::ostream& stream) const;
    int getItemNo() const;
    const std::string& getItemDesc() const;
    float getItemPrice() const;
};

#include "Item.cpp"

#endif //ANTIKEIMENOSTRAFES_PROJECT_ITEM_HPP
