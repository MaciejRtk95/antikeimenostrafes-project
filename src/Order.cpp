#include "Order.hpp"

#include <cstring>
#include "padding.hpp"
#include "searchItem.hpp"

Order::Order()
{
    orderNo = 0;
    totalAmount = 0;
}

Order::Order(int orderNoToSet, const std::string& orderDateToSet, float totalAmountToSet,
        int custNoToSet, const std::string& custNameToSet, const std::string& custEmailToSet,
        int itemNoToSet, const std::string& itemDescToSet, float itemPriceToSet)
{
    orderNo = orderNoToSet;
    orderDate = orderDateToSet;
    totalAmount = totalAmountToSet;
    Customer::setData(custNoToSet, custNameToSet, custEmailToSet);
    Item::setData(itemNoToSet, itemDescToSet, itemPriceToSet);
}

void Order::readData()
{
    Customer::readData();
    std::cout << "And your order information:\n"
              << "    Order number: ";
    std::cin.sync();
    std::cin >> orderNo;
    std::cout << "    Date (dd/mm/yyyy): ";
    std::cin.sync();
    std::cin >> orderDate;
}

void Order::setData(int orderNoToSet, const std::string& orderDateToSet, float totalAmountToSet,
        int custNoToSet, const std::string& custNameToSet, const std::string& custEmailToSet,
        int itemNoToSet, const std::string& itemDescToSet, float itemPriceToSet)
{
    orderNo = orderNoToSet;
    orderDate = orderDateToSet;
    totalAmount = totalAmountToSet;
    Customer::setData(custNoToSet, custNameToSet, custEmailToSet);
    Item::setData(itemNoToSet, itemDescToSet, itemPriceToSet);
}

void Order::printData(std::ostream& stream = std::cout) const
{
    Customer::printData(stream);
    stream << "Order data:\n"
           << "    - Number: " << orderNo << '\n'
           << "    - Order date: " << orderDate << "\n\n";

    unsigned maxLengths[5] = {4, 11, 6, 5, 5}; // Code, Description, Amount, Price, Total
    char floatBuffer[32];
    for (unsigned i = 0; i<items.size(); ++i) {
        unsigned length = std::to_string(items[i].getItemNo()).length();
        if (maxLengths[0]<length)
            maxLengths[0] = length;
        length = items[i].getItemDesc().length();
        if (maxLengths[1]<length)
            maxLengths[1] = length;
        length = std::to_string(quantities[i]).length();
        if (maxLengths[2]<length)
            maxLengths[2] = length;
        sprintf(floatBuffer, "%.2f", items[i].getItemPrice());
        length = strlen(floatBuffer);
        if (maxLengths[3]<length)
            maxLengths[3] = length;
        sprintf(floatBuffer, "%.2f", quantities[i]*items[i].getItemPrice());
        length = strlen(floatBuffer);
        if (maxLengths[4]<length)
            maxLengths[4] = length;
    }

    stream << centrePadding("Code", maxLengths[0]) << ' '
           << centrePadding("Description", maxLengths[1]) << ' '
           << centrePadding("Amount", maxLengths[2]) << ' '
           << centrePadding("Price", maxLengths[3]) << ' '
           << centrePadding("Total", maxLengths[4]) << '\n';
    for (unsigned j = 0; j<5; ++j) {
        for (unsigned i = 0; i<maxLengths[j]; ++i)
            stream << '=';
        if (j!=4)
            stream << ' ';
        else
            stream << '\n';
    }
    for (unsigned i = 0; i<items.size(); ++i) {
        stream << leftPadding(std::to_string(items[i].getItemNo()), maxLengths[0])
               << ' ' << rightPadding(items[i].getItemDesc(), maxLengths[1])
               << ' ' << leftPadding(std::to_string(quantities[i]), maxLengths[2]);
        sprintf(floatBuffer, "%.2f", items[i].getItemPrice());
        stream << ' ' << leftPadding(floatBuffer, maxLengths[3]);
        sprintf(floatBuffer, "%.2f", quantities[i]*items[i].getItemPrice());
        stream << ' ' << leftPadding(floatBuffer, maxLengths[4]) << '\n';
    }
    for (unsigned j = 0; j<5; ++j) {
        for (unsigned i = 0; i<maxLengths[j]; ++i)
            stream << '=';
        if (j!=4)
            stream << '=';
        else
            stream << '\n';
    }
    stream << "\nTotal: " << std::fixed;
    stream.precision(2);
    stream << totalAmount
           << "\nDelivery fee: " << shippingCost();
}

int Order::getOrderNo() const
{
    return orderNo;
}

const std::string& Order::getOrderDate() const
{
    return orderDate;
}

float Order::getTotalAmount() const
{
    return totalAmount;
}

void Order::buyItem(const Item& item, unsigned quantity)
{
    int index = searchItem(items, item.getItemNo());
    if (index!=-1)
        quantities[index] += quantity;
    else {
        items.push_back(item);
        quantities.push_back(quantity);
    }
    totalAmount += quantity*item.getItemPrice();
}

int Order::shippingCost() const
{
    return (200<=totalAmount) ? 0 : 20;
}
