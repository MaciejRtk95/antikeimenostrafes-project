#ifndef ANTIKEIMENOSTRAFES_PROJECT_ORDER_HPP
#define ANTIKEIMENOSTRAFES_PROJECT_ORDER_HPP

#include "Customer.hpp"
#include "Item.hpp"

class Order : public Customer, public Item {
private:
    int orderNo;
    std::string orderDate; // dd/mm/yyyy
    float totalAmount;
    std::vector<Item> items;
    std::vector<unsigned> quantities;

public:
    Order();
    Order(int orderNoToSet, const std::string& orderDateToSet, float totalAmountToSet,
            int custNoToSet, const std::string& custNameToSet, const std::string& custEmailToSet,
            int itemNoToSet, const std::string& itemDescToSet, float itemPriceToSet);

    void readData(); // Καλεί πρώτα την συνάρτηση readData() της Customer
    // και μετά ζητά να πληκτρολογηθεί το orderNo και το orderDate
    void setData(int orderNoToSet, const std::string& orderDateToSet, float totalAmountToSet,
            int custNoToSet, const std::string& custNameToSet, const std::string& custEmailToSet,
            int itemNoToSet, const std::string& itemDescToSet, float itemPriceToSet);
    void printData(std::ostream& stream) const;
    int getOrderNo() const;
    const std::string& getOrderDate() const;
    float getTotalAmount() const;
    void buyItem(const Item& item, unsigned quantity); // Πολλαπλασιάζει την ποσότητα με την τιμή μονάδος του προϊόντος που αγοράστηκε και αθροίζει την αξία στο συνολικό ποσό αγορών
    int shippingCost() const; // Αν το συνολικό ποσό αγορών είναι μεγαλύτερο από 200 ευρώ επιστρέφει 0, αλλιώς επιστρέφει 20 ευρώ μεταφορικά έξοδα
};

#include "Order.cpp"

#endif //ANTIKEIMENOSTRAFES_PROJECT_ORDER_HPP
