#include <fstream>
#include <vector>

int fileToArr(std::ifstream& fin, std::vector<Item>& items)
{
    while (fin.good()) {
        try {
            items.emplace_back(fin);
        }
        catch (const std::exception& error) {
            std::cout << "Error: " << error.what() << '\n'
                      << "Continuing trying to read \"ITEMS.TXT\"..." << std::endl;
        }
    }

    return items.size();
}
