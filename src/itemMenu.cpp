#include <vector>
#include "Item.hpp"
#include "printItems.hpp"

int itemMenu(const std::vector<Item>& items)
{
    int selection;
    printItems(items);
    std::cout << "Add to your order using the appropriate code\n"
    << "or enter 0 to exit: " << std::flush;
    std::cin.sync();
    std::cin >> selection;
    return selection;
}
