#include <iostream>
#include <fstream>
#include <vector>
#include "Customer.hpp"
#include "Item.hpp"
#include "Order.hpp"
#include "fileToArr.hpp"
#include "itemMenu.hpp"
#include "searchItem.hpp"

int main()
{
    std::vector<Item> items;
    items.reserve(100);
    std::ifstream infile("ITEMS.TXT");
    std::ofstream outfile("ORDERS.TXT");

    fileToArr(infile, items);
    infile.close();

    Order order;
    order.readData();

    int selection;
    while (true) {
        std::cout << '\n';
        selection = itemMenu(items);
        if (selection==0)
            break;
        selection = searchItem(items, selection);
        if (selection!=-1) {
            int quantity;
            std::cout << "How many? " << std::flush;
            std::cin.sync();
            std::cin >> quantity;
            if (0<quantity) {
                order.buyItem(items[selection], quantity);
                std::cout << "Added to the order... Now totalling " << order.getTotalAmount() << std::endl;
            }
            else
                std::cout << "Canceled adding this item to the order" << std::endl;
        }
        else
            std::cout << "Error: Product not found" << std::endl;
    }
    order.printData(outfile);

    return 0;
}
