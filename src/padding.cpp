#include <string>

std::string leftPadding(const std::string& source, unsigned targetLength, char padding = ' ')
{
    if (targetLength<=source.length())
        return source;
    return std::string(targetLength-source.length(), padding)+source;
}

std::string rightPadding(const std::string& source, unsigned targetLength, char padding = ' ')
{
    if (targetLength<=source.length())
        return source;
    return source+std::string(targetLength-source.length(), padding);
}

std::string centrePadding(const std::string& source, unsigned targetLength, char padding = ' ')
{
    if (targetLength<=source.length())
        return source;
    unsigned difference = targetLength-source.length(), half = difference/2;
    if (difference%2)
        return std::string(half,padding)+source+std::string(half+1, padding);
    else
        return std::string(half,padding)+source+std::string(half, padding);
}
