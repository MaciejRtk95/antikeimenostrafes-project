#include <iostream>
#include <vector>
#include <cstring> // For converting floats and controlling the format
#include "Item.hpp"
#include "padding.hpp"

void printItems(const std::vector<Item>& items, std::ostream& stream = std::cout)
{
    unsigned maxNumberLength = 0, maxDescriptionLength = 0, maxPriceLength = 0;
    std::vector<std::string> numbers(items.size()), prices(items.size());
    char priceBuffer[32];

    for (int i = 0; i<items.size(); ++i) {
        numbers[i] = std::to_string(items[i].getItemNo());
        if (maxNumberLength<numbers[i].length())
            maxNumberLength = numbers[i].length();

        if (maxDescriptionLength<items[i].getItemDesc().length())
            maxDescriptionLength = items[i].getItemDesc().length();

        sprintf(priceBuffer, "%.2f", items[i].getItemPrice());
        prices[i] = priceBuffer;
        if (maxPriceLength<prices[i].length())
            maxPriceLength = prices[i].length();
    }

    stream << "Our variety of \"makaronakia\":\n";
    unsigned totalLineLength = maxNumberLength+maxDescriptionLength+maxPriceLength+6;
    for (int i = 0; i<totalLineLength; ++i)
        stream << '-';
    stream << '\n';
    for (int i = 0; i<items.size(); ++i) {
        stream << leftPadding(numbers[i], maxNumberLength)
               << "  " << rightPadding(items[i].getItemDesc(), maxDescriptionLength)
               << "    " << leftPadding(prices[i], maxPriceLength) << '\n';
    }
    for (int i = 0; i<totalLineLength; ++i)
        stream << '-';
    stream << '\n';
}
