#include <vector>
#include "Item.hpp"

int searchItem(const std::vector<Item>& items, const int searchCode)
{
    for (int i=0; i<items.size(); ++i) {
        if (items[i].getItemNo()==searchCode)
            return i;
    }
    return -1;
}

